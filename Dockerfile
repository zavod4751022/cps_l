FROM gitlab.iteco.mobi:4567/cps/cps_dummy/openjdk:21-ea-17-jdk-buster
COPY build/libs/cps*.jar /app/cps_dummy.jar
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/cps_dummy.jar"]
