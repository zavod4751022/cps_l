- `./gradlew bootRun` - try app
- `./gradlew bootJar` - build uber-jar in projectDir/build/libs
- `./gradlew jar` - build jar in projectDir/build/libs
- `./gradlew test` - run tests
- `java -jar build/libs/appName.jar` - launch builded app(docker enviroment not creating)

Api endpoints:
- `GET /`
- `POST /perform_login`
- `POST /logout`

App default url: localhost:8080\
App username: root\
App password: root






