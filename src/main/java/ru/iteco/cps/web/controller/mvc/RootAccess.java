package ru.iteco.cps.web.controller.mvc;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class RootAccess {

    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String rootAccess(){
        return "root";
    }

}
