#!/usr/bin/env bash
mkdir ./k8s/.generated/
REF=${1}
TAG=${2}
export BUILD_NUMBER=${TAG}
export BUILD_REF=${REF}
for f in ./k8s/deploy/*.yaml
do
  envsubst < $f > "./k8s/.generated/$(basename $f)"
done

kubectl apply -f ./k8s/.generated/
kubectl -n cps scale --replicas=0 deployment cps-dummy
kubectl -n cps scale --replicas=1 deployment cps-dummy
